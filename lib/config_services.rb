require "yaml"

class ConfigClient
  @values 

  attr_reader :values

    def initialize(profile_str)
      @values = getConfigs(profile_str) 
    end

    def getConfigs(var_profile_str)
      pwd=Dir.pwd
      configurations = nil
      profile_str = var_profile_str.sub("__PWD__",pwd)
      if profile_str.start_with? "file://"
        profile_str = profile_str.sub("file://","")
        if profile_str.end_with? "yml" or profile_str.end_with? "yaml" 
        configurations = YAML.load_file(profile_str)
        end
      end
      return configurations
    end
end