Gem::Specification.new do |s|
  s.name        = 'config_services'
  s.version     = '0.0.2'
  s.date        = '2018-09-01'
  s.summary     = "Config services: client and server!"
  s.description = "lib and server for configuration management"
  s.authors     = ["Erik Beltran"]
  s.email       = 'erik.beltran.e@gmail.com'
  s.files       = ["lib/config_services.rb"]
  s.homepage    =
    'http://ebeltran.pw/gems'
  s.license       = 'MIT'
end